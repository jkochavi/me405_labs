'''
@file touchPanelCalibration.py

@brief Contains a calibration script that determines the center and resolution of 
any given 4-wire touch panel.

@details This script calculates the center and resolution of a 4-wire touch panel
using the touchPanel module for Lab0x07. To use this calibration tool, the 
user must know the width and length of the active area of the touch panel,
in millimeters. They must alter the tuple containing these dimensions before
using the program. By default, the panel dimensions are (176, 100), which
are the dimensions of the active area for the touch panel used in Lab0x07.
In this script, the user identifies the ADC bounds of the panel by iteratively
touching each corner of the panel, following instructions provided by the 
serial monitor. When the user is satisfied with the corner values, they 
can press the blue user button to move on to the next corner When all four
corners have been touched, the script calculates the center coordinates and
panel resolution based on the outer bounds obtained by the user. The 
center and resolution is returned to the user before exiting the program.

@author Jordan Kochavi

@copyright 2021

@date March 3, 2021 Original file.

@section sec1 Hardware setup
My hardware setup for this lab is shown in the picture below. I annotated
over the picture the orientations of the X and Y axes, as documented in the 
touch panel's datasheet. The datasheet can be found <a href="https://www.buydisplay.com/8-inch-4-wire-resistive-touch-screen-panel"><b>here.</b></a>

<img src="https://bitbucket.org/jkochavi/me405_labs/raw/c9d270ce89f0233bea87ea27b5c4b5136ac2b5ae/Lab7/HardwareSetup.png" alt="Hardware Setup">

After I made the class for the touch panel, I created this calibration script
to determine the outer bounds of the ADC readings, which would help me calculate
the panel center and resolution. I tried this out manually, and kept note of the
ADC values at the four corners of the panel on a whiteboard. The readings I got
are visible in the picture below. 

<img src="https://bitbucket.org/jkochavi/me405_labs/raw/c9d270ce89f0233bea87ea27b5c4b5136ac2b5ae/Lab7/whiteboard.jpeg" alt="Whiteboard" size=500>

From the picture above, the readings from each corner, in the format (X, Y), were:
* Bottom Left: (3700, 220)
* Top Left: (500, 220)
* Top Right: (500, 3800)
* Bottom Right: (3700, 3800)

From these readings, it appears that the X and Y axis on the panel should be flipped. 
In addition, it appears that the Y axis (which is labeled as X in the picture above), is inverted.
Because of these results, I modified the calculate() method for the touchPanel class to 
flip the axes and invert the Y axis, so the results would match the product's datasheet. 
It's also possible that some of the pins weren't energized correctly, which resulted in the
readings being flipped an inverted. However, the module ultimately returns accurate coordinates in millimeters
and in the right orientation.   

After calculating the center point and resolution manually, I made this script
to automate the process. I found that both methods were comparable. When I did it manually,
I found a center of (2010, 2100), and this script returned a center of (1997, 2080). Both
methods calculated a resolution of (20, 32) [counts/mm].

@section sec2 Video of test results
The final results can be viewed in <a href="https://www.youtube.com/watch?v=yNskx-_xkE0&feature=youtu.be"><b>this Youtube video.</b></a>
In this video, I am running a small piece of test code to continuously output
the measured coordinates. The test code uses a while-loop, as shown in the code block below. 

    while True:
      try:
          if panel.readZ():
              coordinates = panel.read()
              print(coordinates)
              utime.sleep_ms(100)
      except KeyboardInterrupt:
          break

If the panel is touched, then this test code reads the full coordinates using
the method, read(). As shown in the video, touching the corners of the panel
return coordinates to the Putty terminal that are varying signs of around (87.5, 50).
Since the active area dimensions of the panel are 176 and 100 mm, then relative to the
center of the screen, the corners should be 88 and 50 mm away. The corners of the
panel map relatively accurately. 

@section sec3 Time measurements
To measure the time required to take each measurement, I used the utime library.
The test code below calculates the average elapsed time across 100 measurements.

     elapsedTime = 0
     for k in range(100):
         start_time = utime.ticks_us()
         vals = panel.read()
         end_time = utime.ticks_us()
         inc = end_time-start_time
         elapsedTime += inc
     elapsedTime /= 100
     print("Average time to read all 3 values [us] "+str(elapsedTime))

The line, val = panel.read() is reassigned to panel.readX(), panel.readY(),
and panel.readZ() to obtain average elapsed time measurements for those methods.
After testing all four methods, the results are shown below:
* panel.readX()  - 452 microseconds
* panel.readY()  - 453 microseconds
* panel.readZ()  - 450 microseconds
* panel.read()     - 987 microseconds

Since the individual measurements take less than 500 microseconds to return,
the methods will be suitable for cooperative code on the rest of the project.
In addition, the combined read method takes less 1500 microseconds to return,
which should also be suitable for the rest of the project.
'''

## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the module used to interface with the touch panel.
from touchPanel import touch_panel
## @brief Import the micropython module used to measure time and implement delay functions.
import utime
## @brief A variable to store the current corner being calibrated
buttonCount = 1
## @brief A boolean that toggles True when the blue user button is pressed
pressed = False
'''
@brief   An interrupt service routine for the on-board button.
@details This ISR is triggered on the falling edge of GPIO pin PC13, which
         is attached to the on-board button. The routine stores the current
         timer count in the global variable, endCount. 
@param param An unused parameter.
@return This function does not return anything, as it is a callback function.
'''
def button_isr(param):   # ISR definition
    global pressed       # Reference global variable
    if not pressed:      # If the button hasn't been pressed yet...
        pressed = True   #     Toggle true
## @brief Set up a hardware interrupt, attached the falling edge of hardware pin PC13. 
extint = pyb.ExtInt(pyb.Pin.board.PC13,   # GPIO pin PC13
         pyb.ExtInt.IRQ_FALLING,          # Interrupt on falling edge
         pyb.Pin.PULL_UP,                 # Activate pullup resistor
         button_isr)                      # Interrupt service routine
## @brief Hardware pin attached to panel pin X+
xp = pyb.Pin(pyb.Pin.board.PA7)
## @brief Hardware pin attached to panel pin X-
xm = pyb.Pin(pyb.Pin.board.PA1)
## @brief Hardware pin attached to panel pin Y+
yp = pyb.Pin(pyb.Pin.board.PA6)
## @brief Hardware pin attached to panel pin Y-
ym = pyb.Pin(pyb.Pin.board.PA0)
'''
@brief   Dimensions of the screen, in millimeters.
@details This tuple stores the width and height of the panel in the form,
         (width, height), where width and height are in millimeters. Depending
         on the touch panel being used, this tuple should be modified before 
         starting calibration.
'''
dimensions = (176,100)

'''
@brief   Create touch panel object.
@details Here, we create a touch panel object, but without passing parameters for
         the panel resolution and center. Since the calibration tool determines
         these values, we can just apply the default values in the construtor.
'''
panel = touch_panel((xp, xm, yp, ym),dimensions)

## @brief State variable for the FSM.
state = 0
'''
@brief   List that contains the ADC values at each of the four corners.
@details This list stores the ADC values, X and Y, at each of the four corners.
         The variables are stored in the form, [X1, Y1, X2, Y2, X3, Y3, X4, Y4],
         where corner 1 is the bottom left corner, corner 2 is the top left corner,
         corner 3 is the top right corner, and corner 4 is the bottom right corner.
'''
corners = [0,0,0,0,0,0,0,0] # [X1, Y1, X2, Y2, X3, Y3, X4, Y4]

# Print welcome message
print("The following code contains a calibration method for a touch panel")
print("This calibration method determines the center and resolution of the panel using known width and length dimensions, in millimeters.")
print("Screen dimensions: Width, "+str(dimensions[0])+", Height, "+str(dimensions[1]))

while True:                                                                       # Run FSM in forever loop
    try:                                                                          # As long as there are no keyboard interrupts...
        if not state:                                                             # State 0: Init state
            pressed = False                                                       # Init to false
            state = 1                                                             # Transtion to state 1
        elif state == 1:                                                          # State 1: Display instructions
            if buttonCount == 1:                                                  # If we're on corner 1...
                print("Press and hold the panel in the bottom left corner.")      #    Then prompt bottom left corner
            elif buttonCount == 2:                                                # Elif we're on corner 2...
                print("Press and hold the panel in the top left corner.")         #    Then prompt top left corner
            elif buttonCount == 3:                                                # Elif we're on corner 3...
                print("Press and hold the panel in the top right corner.")        #    Then prompt top right corner
            elif buttonCount == 4:                                                # Elif we're on corner 4...
                print("Press and hold the panel in the bottom right corner.")     #    Then prompt bottom right corner
            print("Press the blue button when satisfied with the ADC values.")    # Button instructions
            state = 2                                                             # Transition to state 2
        elif state == 2:                                                          # State 2: Read raw ADC values
            if panel.readZ():                                                     # If the panel has been touched...
                coordinates = panel.readRaw()                                     #   Then retrieve the pressed coordinates
                print("X: "+str(coordinates[0])+", Y: "+str(coordinates[1]))      #   Print to user 
                utime.sleep_ms(100)                                               #   Delay 100 ms
            elif pressed:                                                         # Elif the button was pressed...
                state = 3                                                         #   Transition to state 3
        elif state == 3:                                                          # State 3: Store coordinate values
            xindex = 0                                                            # Init storing index to zero (for buttonCount = 1)
            if buttonCount == 2:                                                  # If buttonCount = 2...
                xindex = 2                                                        #   Then store to index 2
            elif buttonCount == 3:                                                # Elif buttonCount = 3...
                xindex = 4                                                        #   Then store to index 4 
            elif buttonCount == 4:                                                # Elif buttonCount = 4...
                xindex = 6                                                        #   Then store to index 6
            yindex = xindex+1                                                     # Yindex is directly after the Xindex
            corners[xindex] = coordinates[0]                                      # Store x coordinate of corner ADC value
            corners[yindex] = coordinates[1]                                      # Store y coordinate of corner ADC value
            buttonCount += 1                                                      # Increment buttonCount
            if buttonCount > 4:                                                   # If buttonCount > 4, we're done calibrating 
                state = 4                                                         #   Transition to state 4
            else:                                                                 # Otherwise...
                pressed = False                                                   #   Lower pressed flag
                state = 1                                                         #   Display next instructions
        elif state == 4:                                                          # State 4: Calculate and display results
            leftBound = round((corners[1]+corners[3])/2)                          # Calculate ADC bound at left of panel
            rightBound = round((corners[5]+corners[7])/2)                         # Calculate ADC bound at right of panel
            upBound = round((corners[2]+corners[4])/2)                            # Calculate ADC bound at top of panel
            downBound = round((corners[0]+corners[6])/2)                          # Calculate ADC bound at bottom of panel
            centerY = round((leftBound+rightBound)/2)                             # Calculate center X based on left and right bounds
            centerX = round((upBound+downBound)/2)                                # Calculate center Y based on up and bottom bounds
            resX = round((rightBound - leftBound)/dimensions[0])                  # Calculate resolution based on panel width
            resY = round((downBound - upBound)/dimensions[1])                     # Calculate resolution based on panel height
            print(corners)                                                        # Print the obtained corners
            print("Center [X,Y], in ADC counts: ("+str(centerX)+","+str(centerY)+")")              # Print center 
            print("Resolution [X,Y], in ADC counts per millimeter: ("+str(resX)+","+str(resY)+")") # Print resolution
            break                                                                 # Exit loop
    except KeyboardInterrupt:                                                     # If keyboard interrupt (cntl+c)...
        break                                                                     # Break out of loop to exit program 
# Exit program    
print("Calibration finished")
print("You can use these center and resolution values for calling the calibrated touch panel functions.")
print("Press cntl+d to begin again.")