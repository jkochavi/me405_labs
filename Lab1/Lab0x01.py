'''
@file Lab0x01.py
@brief Contains a finite state machine model of a vending machine.

@details This file contains the implementation of a finite state machine model
of a vending machine called Vendotron. The vending machine contains four
different drink options, and the user can insert any number of combonations
of different monetary denominations, ranging from pennies to $20 bills. 
The user provides input to the machine using the keyboard, which this file
interfaces with using a keyboard module. The machine provides feedback
to the user in the terminal/console that this script is run in.

@section sec1 FSM Overview

The FSM runs in a forever loop. This loop
will run until the user types Cntl-C to exit the python script. A state 
transition diagram is shown below.

<img src="https://bitbucket.org/jkochavi/me405_labs/raw/0aad6ec42ed133beb59293671f12a203a571ff65/Lab1/StateTransitionLab1.png" width="500px">

<b>State 0</b>


This is the init state that the machine enters upon startup. Its
only function is to display the welcome message and instructions,
and then transition into the main/mastermind state. 

<b>State 1</b>


This is our main/mastermind state. The FSM will remain here until
the user does something. If any key is pressed, the state takes
appropriate action and transitions states when needed. The
keyboard library used for this script does not allow for listening
to all number presses. Instead, we have to check if every key was 
pressed. To do this more efficiently, we have one state for all 
number presses, and one state for all drink presses. This is 
accomplished with two buffers: @c intBuff and @c strBuff. The 
former is a global variable of type integer, that corresponds to 
the index value of each coin or bill. This way, the number-handling
state can be made general, and use intBuff for all calculations. 
When intBuff is not set, A.K.A a coin has not been inserted, its
default value is -1, because it is impossible for the variable
to get to -1 on its own without being forced. Similarly, strBuff
is set as empty when a drink has not been selected, or the user
did not press the eject button.

<b>State 2</b>


We enter this state when a user adds a coin or bill. The list that
stores the current payment/balance is indexed in the same order 
that keys are pressed, i.e, 0 -> pennies, payment[0] is the number
of pennies inserted into the machine by the user. So when a coin is
inserted, we just add one to the appropriate index of payment[].
Next, we call the function @c getBalance(), to calculate a balance
that can be displayed to the user in an intuitive way. We want to 
show them "3.75", instead of how many of each coin they put in. 
getBalance() combines the list, payment[] into a single number.
Finally, the balance is displayed to the user, and intBuff is reset
to receive the next coin or bill. Then, we return to state 1.
We enter this state when a user selects a drink. They can type
C, P, S, or D. This state uses a global variable of form tuple,
called @c prices. This stores the current prices, in cents, of
each drink option. Based on which drink is selected, the function
getChange() is called, which calculates the change to be returned
to the user in the minimum amount of denominations. This function
will return None if an insufficient payment is provided. If 
the user lacks sufficient funds, we transition back to state 1,
so they can input more coins. If the user provides exact change,
then the balance is returned to zero, and the we return to state 1
to receive more coins. If the user has change leftover, then we 
transition to state 4, to prompt them for an additional beverage.

<b>State 4</b>


We enter this state when the user still has a balance left
after selecting a drink. In this state, we display the remaining
balance to the user, and prompt the user to select another drink,
or press E to eject the remaining balance. 

<b>State 5</b>


This is the eject state. When the user presses "E", the machine
returns the balance. To simulate this, we return the balance to
zero, and display a new welcome message so the user can load 
another balance.

<b>Key Press State</b>


We enter this state while waiting for a key to be released.
The keyboard library function, is_pressed(), can allow dozens
of presses within milliseconds, and we only want one press at a
time. So, after a press is detected, we wait in this state, until
all buttons are no longer pressed.
            
@author Jordan Kochavi

@date January 20, 2021
'''

## @brief   Module used to listen to the keyboard.
#  @details This module must be installed using the pip3 install command.
import keyboard

## @brief   A list of all the user's current payment in the machine.
#  @details This variable stores how many of each denomination has been 
#           inserted into the machine. The 0 index corresponds to the number
#           of pennies inserted, and the 7 index corresponds to the number of 
#           $20 bills inserted. The middle indeces range from pennies to $20
#           bills in increasing order.
payment = [0,0,0,0,0,0,0,0]

## @brief   A list of all the user's change after making a purchase.
#  @details This variable stores how many of each denomination will be 
#           returned to the user. It is ordered in the same format as 
#           @c payment.
change = [0,0,0,0,0,0,0,0]   #

## @brief   A tuple containing the prices of each drink.
#  @details The variable contains the prices of each drink. Each element of 
#           this tuple corresponds to an integer value, in cents, of a drink
#           price. Values can easily be changed here as needed, should drink
#           prices need to change later on.
prices = (100,120,85,110)    # A tuple containing the prices, in cents, of each drink

## @brief   A dynamic variable for storing which letter key was pressed.
#  @details This variable keeps track of which letter key was pressed. This
#           alleviates some work in state 1, which can store a key press in a 
#           variable like this, and pass it onto a different state, which 
#           interprets its value and acts on it.
strBuff = ""                 # String variable used to store which letter key was pressed.

## @brief   A dynamic variable for storing which number key was pressed.
#  @details This variable keeps track of which number key was pressed. This
#           alleviates some work in state 1, which can store a key press in a 
#           variable like this, and pass it onto a different state, which 
#           interprets its value and acts on it.
intBuff = -1                 # Integer variable used to store which number key was pressed.
                             
## @brief   Integer that corresponds to FSM state where we wait for key-release.
waitForRelease = 7

## @brief   State variable for the FSM.
#  @details This variable stores the current state of the FSM.
state = 0

## @brief   Computes change for monetary transaction.
# @details Receives a given item price, and payment, and calculates exact
#         change. Payment can be given in a range of coins and bills, 
#         ranging from pennies to $20 bills. First, the function creates
#         a local tuple variable. This variable, called transforms, 
#         contains a list of coefficients that convert each denomination
#         to cents. For example, transforms[5] = 500, which corresponds
#         to the transform coefficient for a 5 dollar bill--a 5 dollar bill
#         is equivalent to 500 cents. Next, the function uses a for-loop
#         to combine the input parameter, payment,into a single integer 
#         value, which represents the payment in cents. This is stored in
#         the local variable, payment_int, which is subsequently compared
#         to the input item price. Subtracting the payment from the price
#         calculates how much change we need to return. We use another 
#         for-loop to iterate through each denomination, but this time in 
#         reverse. Doing this process in reverse lets us return the payment
#         in as few denominations as possible, since larger denominations
#         are at the end of the payment tuple. The loop uses floor division
#         to calculate how many units of each denomination can fit in the
#         remaining change. Finally, the change is returned as a tuple.
# @param   price An intager representing the price of an item, in cents.
# @param   payment A tuple representing the integer number of pennies,
#         nickels, dimes, quarters, dollars, fives, tens and twenties.
# @return  A tuple representing the change computed, if funds are
#          sufficient.
def getChange(price, payment):
    try:            # Check if price is in the correct format...
        price += 1  # Add 1
        price -= 1  # Subtract 1
    except:         # If the price is not in fact, right, then return early
        return None
    transforms = (1,5,10,25,100,500,1000,2000) # Tuple for converting change
    # Transpose payment to integer number of pennies
    payment_int = 0                                     # Init to 0
    for k in range(8):                                  # Do this 8 times...
        try:                                            # Make sure it's possible
            payment_int += payment[k]*transforms[k]     # Multiply by coefficient
        except:                                         # If something went wrong...
            payment_int = -1                            # Make the result impossible
            break                                       # Get out of the loop
    if payment_int >= price:                            # If exact or excess payment...
        change_int = payment_int - price                # Calculate difference
        change_list = []                                # Init empty list
        index = 7                                       # Init to 7 (max index)
        for k in range(8):                              # Do this 8 times...
            temp_change = change_int//transforms[index] # Floor divide by transform
            change_list.insert(0,temp_change)           # Insert temporary value
            change_int -= temp_change*transforms[index] # Calculate remaining change
            index -= 1                                  # Decrement index
        return tuple(change_list)                       # Return a tuple of change
    else:                                               # If something went wrong...
        return None                                     # Return None

## @brief   Displays a welcome message to the user..
# @details  This function displays a welcome message, and the drink prices of
#           each drink in the vending machine. This function uses the print
#           command to display the message and prices to the console.
def printWelcome():
    print("\nWelcome to Vendotron^TM^!")
    print("|----Featured Drinks----|")
    print("|----Cuke----------$1.00|")
    print("|----Popsi---------$1.20|")
    print("|----Spryte--------$0.85|")
    print("|----Dr. Pupper----$1.10|\n")

## @brief  Displays a series of keyboard input options.
# @details This function displays every allowable keyboard input to the user.
#          Allowable inputs include letters and numbers. Letters correspond
#          to drink choices, and numbers correspond to which type of bill or
#          coin is inserted into the machine to add toward a purchase. This
#          function uses the print command to display the instructions in the
#          console.
def displayInstructions():
    print("Instructions:")
    print("Press:")
    print("E -> Eject")
    print("C -> Cuke")
    print("P -> Popsi")
    print("S -> Spryte")
    print("D -> Dr. Pupper")
    print("\nPayment:")
    print("Press:")
    print("0 -> Penny")
    print("1 -> Nickle")
    print("2 -> Dime")
    print("3 -> Quarter")
    print("4 -> $1 bill")
    print("5 -> $5 bill")
    print("6 -> $10 bill")
    print("7 -> $20 bill\n")

## @brief   Computes tuple into readable monetary value.
# @details  Receives a given payment in the form of a tuple, and calculates
#           a money value based on the number of denominations provided. This
#           function is necessary for displaying change and remaining balances
#           to the user. When we return their change, we want to show them
#           how many of each coin or bill we return to them, but it is also
#           helpful to know the $ amount in the form of $XX.XX. This value
#           is calculated using a tuple of transforms, that we multiply by 
#           the payment to calculate a single number, in cents. Next, the 
#           function converts that integer value to a string, where we can
#           construct a new string with the "$" sign, and the "." that
#           separates dollars and cents. For more information about how the
#           join() and map() functions were used to combine a list into a 
#           single string, refer to this online thread:
#           https://www.decalage.info/en/python/print_list
# @param    payment A list or tuple representing the amount currently paid
#           or being returned to the user.
# @return   A string representing the current balance or change returned, 
#           in the form of $XX.XX. 
def getBalance(payment):
    
    transforms = (1,5,10,25,100,500,1000,2000) # Tuple for converting change
    balance = 0                                # Initialize local var to 0
    for k in range(8):                         # Do this 8 times...
        balance += payment[k]*transforms[k]    #    Multiply each coin by transform
    strBalance = str(balance)                  # Convert integer balance to string
    lstBalance = []                            # Initialize empty list
    for k in range(len(strBalance)):           # Add each character in the new string
        lstBalance.append(strBalance[k])       #   to the new empty list
    if len(lstBalance) == 2:                   # If the list is only 2 characters long...
        lstBalance.insert(0,"0")               #   Then balance < 100 cents, add a 0 so it reads, 0XX
    elif len(lstBalance) == 1:                 # Else if the list is only 1 character long...
        lstBalance.insert(0,"0")               #   Then balance < 10 cents, add a 00 so it reads, 00XX
        lstBalance.insert(0,"0")               #
    lstBalance.insert(0,"$")                   # Next, insert a $, so it reads, $0XX or $00XX or $XXXX....
    lstBalance.insert(-2,".")                  # Next, insert a ".", so it reads $00.XX or $XX.XX, etc...
    strBalance = "".join(map(str,lstBalance))  # Finally, create new string that joins the new list together
    return strBalance                          # Return the new string
    
while True:
    if state == 0:
        printWelcome()         # Call the welcome message function
        displayInstructions()  # Call the instructions message function
        state = 1              # Transition to main state
    elif state == 1:
        if intBuff != -1:                # If a coin or bill has been inserted...
            state = 2                    #    Transition to state 2
            payment = list(payment)      #    Make sure that payment is in list form
        elif strBuff != "":              # If a letter has been pressed...
            if strBuff == "e":           #    If that letter was E...
                change = payment         #       Then the change ejected is what has been paid
                state = 5                #       Transition to eject state
            else:                        #    Otherwise...
                state = 3                #       Transition to the drink state
        elif keyboard.is_pressed("0"):   # Else if 0 was pressed...
            intBuff = 0                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("1"):   # Else if 1 was pressed...
            intBuff = 1                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("2"):   # Else if 2 was pressed...
            intBuff = 2                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition wait-for-release state
        elif keyboard.is_pressed("3"):   # Else if 3 was pressed...
            intBuff = 3                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("4"):   # Else if 4 was pressed...
            intBuff = 4                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("5"):   # Else if 5 was pressed...
            intBuff = 5                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("6"):   # Else if 6 was pressed...
            intBuff = 6                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("7"):   # Else if 7 was pressed...
            intBuff = 7                  #    Assign intBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("e"):   # Else if E was pressed...
            strBuff = "e"                #    Assign strBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("c"):   # Else if C was pressed...
            strBuff = "c"                #    Assign strBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("p"):   # Else if P was pressed...
            strBuff = "p"                #    Assign strBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("s"):   # Else if S was pressed...
            strBuff = "s"                #    Assign strBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
        elif keyboard.is_pressed("d"):   # Else if D was pressed...
            strBuff = "d"                #    Assign strBuff accordingly
            state = waitForRelease       #    Transition to wait-for-release state
    elif state == 2:
        payment[intBuff] += 1              # Add 1 to the appropriate index
        balance = getBalance(payment)      # Call getBalance() using the user payment
        print("Current Balance: ",balance) # Display to the user
        intBuff = -1                       # Reset int buffer
        state = 1                          # Return to state 1
    elif state == 3:
        if strBuff == "c":                        # If the user pressed C...
            change = getChange(prices[0],payment) #    Then, use the Cuke price
        elif strBuff == "p":                      # Else if P is pressed...
            change = getChange(prices[1],payment) #    Then, use the Popsi price
        elif strBuff == "s":                      # Else if S is pressed...
            change = getChange(prices[2],payment) #    Then, use the Spryte price
        elif strBuff == "d":                      # Else if D is pressed...
            change = getChange(prices[3],payment) #    Then, use the Dr. price
        else:                                     # If some other key was allowed somehow...
            state = 1                             #    Then, return to state 1
            strBuff = ""                          #    Clear the string buffer
        if change == None:                                    # If insufficient funds...
            balance = getBalance(payment)                     #    Calculate balance
            print("\nInsufficient Funds... Please add more!") #    Display error message
            print("Current Balance: ",balance)                #    Print current balance
            state = 1                                         #    Return to state 1
            strBuff = ""                                      #    Clear string buffer
        elif change == (0,0,0,0,0,0,0,0):                     # Else if exact change...
            payment = change                                  #    Clear the balance
            print("\nEnjoy your beverage...")                 #    Display message
            print("Current Balance: $0.00")                   #    Display zero balance
            state = 1                                         #    Return to state 1
            strBuff = ""                                      #    Clear string buffer
        else:                                                 # Else if excess...
            state = 4                                         #    Transition to state 4
    elif state == 4:
        payment = change                                     # Calculate new balance, in coins
        balance = getBalance(payment)                        # Calculate new balance, in $ value
        print("\nCurrent Balance: ",balance)                 # Print current balance
        print("Select another drink, or press E to exit...") # Display next prompt
        state = 1                                            # Return to state 1
        strBuff = ""                                         # Reset string buffer
    elif state == 5:
        balance = getBalance(change)                # Get new balance based on payment
        if balance == "$0.00":                      # If balance is already zero...
            print("Nothing to eject!")              #    Then don't bother with the rest
        else:                                       #
            print("\nEjected remaining balance...") # Display message to user
            print("Pennies....",change[0])          # Number of pennies returned
            print("Nickles....",change[1])          # Number of nickles returned
            print("Dimes......",change[2])          # Number of dimes returned
            print("Quarters...",change[3])          # Number of quarters returned
            print("$1 Bills...",change[4])          # Number of $1 bills returned
            print("$5 Bills...",change[5])          # Number of $5 bills returned
            print("$10 Bills..",change[6])          # Number of $10 bills returned
            print("$20 Bills..",change[7])          # Number of $20 bills returned
            print("Returned Change: ",balance)      # Display single $ returned 
        payment = [0,0,0,0,0,0,0,0]             # Reset payment
        strBuff = ""                            # Reset string buffer
        printWelcome()                          # Print the welcome message
        state = 1                               # Return to state 1
    elif state == waitForRelease:
        if not keyboard.is_pressed("0") \
        and not keyboard.is_pressed("1") \
        and not keyboard.is_pressed("2") \
        and not keyboard.is_pressed("3") \
        and not keyboard.is_pressed("4") \
        and not keyboard.is_pressed("5") \
        and not keyboard.is_pressed("6") \
        and not keyboard.is_pressed("7") \
        and not keyboard.is_pressed("e") \
        and not keyboard.is_pressed("c") \
        and not keyboard.is_pressed("p") \
        and not keyboard.is_pressed("s") \
        and not keyboard.is_pressed("d") \
        and not keyboard.is_pressed("q") \
        and not keyboard.is_pressed("i"): 
                state = 1                     # Return to state 1          
    else:
        pass                                  # This state shouldn't exist...