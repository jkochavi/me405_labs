## @file main.py
#  Measures a step response by reading the voltage output of a built-in button. 
#
#  This script communicates with an interface on a PC using a serial connection.
#  When the interface tells the Nucleo-L476RG development board to begin taking
#  readings, this script will use a built-in ADC to read the voltage across
#  a built-in button. When the user presses the button, the readings will 
#  record a step response, in ADC counts. These readings are reported back to
#  the interface for displaying to the user. To setup the hardware to work with
#  this script, a jumper wire was used to connect pins PC13 and PA0, as shown
#  in the picture below. The built-in button is attached to PC13, and PA0 will
#  be setup as an input to read the step response. To load this file onto the 
#  Nucleo-L476RG, open the Anaconda command line and type: 
#  ampy --port <device port> put main.py, where <device port> is 
#  whichever serial port the Nucleo is plugged into, i.e. COM4. Ensure that 
#  this file is the command line's working directory. The output of this 
#  program can be viewed in any serial monitor, such as Putty. 
#
#  @author Jordan Kochavi
#
#  @copyright 2021
#
#  @date February 1, 2021
#
#

## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import the UART serial communication module used to communicate with the PC.
from pyb import UART
## @brief Import module used to create a buffer of ADC readings.
import array

## @brief Boolean that notes when the blue button has been pressed.
pressed = False

## @brief Create pin object for ADC pin PA0.  
ADCpin = pyb.Pin(pyb.Pin.board.PA0) # Configure for input

## @brief Create ADC object for reading ADsC values.
ADCreader = pyb.ADC(ADCpin)

## @brief   Frequency at which ADC readings are taken.
#  @details Having the timer frequency stored in a variale allows the user to 
#           easily change it. The variable is set to 200000, which represents
#           200KHz.
frequency = 200000 # Hz

## @brief   ADC threshold before storing to buffer begins.
lowerThreshold = 5

## @brief   ADC threshold before storing to buffer ends.
upperThreshold = 4065

## @brief   Main buffer for storing ADC measurements
#  @details This variable uses the array module to create initialize an array
#           of 1000 elements. This array is used a buffer, and filled using the
#           ADC function, read_timed(), which will fill it at a given timer 
#           frequency. This is the 'main' buffer, which is filled when the 
#           microcontroller detects a step response.  
ADCbuff = array.array("H", (0 for index in range(1000)))

## @brief   A smaller buffer for storing ADC measurements
#  @details Similar to ADCbuff, this variable is filled using the function
#           read_timed(). However, this buffer is constantly filled and refilled
#           in an attempt to catch the rising edge of a step response as quickly
#           as possible. If the state machine detects that this buffer has been
#           filled by a step response, then it fills the main buffer to catch
#           the rest of it.
preBuff = array.array("H", (0 for index in range(500)))

## @brief   A list of timestamps.
#  @details This variable stores a list of timestamps based on the frequency
#           of ADC readings taken. After a reading is taken, a linearly-space
#           time domain is stored here based on the timer frequency. For example,
#           ADC readings taken at 200KHz are taken at a period of 5 microseconds.
#           Therefore, this variable would contain a list of numbers that increment
#           by 5 microseconds.
time = [0]

## @brief   Initialize the on-board LED that is attached to GPIO pin PA5.
onBoardLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP) # Configure for output, pullup
onBoardLED.low()                                             # Initialize LED to off

## @brief A UART port object for communication with PC. 
PCcom = UART(2)

## @brief   Variable used to concatenate the two ADC buffers together.
#  @details After the two buffers, preBuff and ADCbuff are filled, we need to 
#           convert them to standard lists before transmitting them over serial.
#           The arrays are combined using a for-loop, and stored in this variable.
#           This variable is subsequently combined with the time domain, and
#           batch-sent to the PC for storing and plotting.
exportADC = []

## @brief   Create a timer object for timer hardware 2. 
tim2 = pyb.Timer(2, freq=frequency)                            

## @brief   Callback that toggles the status of the green LED.
#  @details This function is called everytime Timer 1 overflows, which will
#           toggle the status of the built-in LED. This toggling is used to 
#           provide feedback to the user that the program is operating normally.
#           During normal operation, the light will blink slowly, every 1 second.
#           However, after the user has pressed the button, it will blink quickly
#           while the Nucleo transmits its data back to the PC. When it is done
#           transmitting, then the LED returns to blinking slowly
# @param timer An unused parameter.
# @return This function does not return anything, as it is a callback function.
def LEDcallBack(timer):                 # Function definition
    if onBoardLED.value():              # If the LED was already on...
        onBoardLED.low()                #    Then turn it off 
    else:                               # Otherwise...
        onBoardLED.high()               # Turn it on

## @brief  Create a timer object for timer hardware 1
tim1 = pyb.Timer(1, freq=1)             # Timer 1 running at 1 Hz
tim1.callback(LEDcallBack)              # Assign callback function accordingly

## @brief   An interrupt service routine for the on-board button.
#  @details This ISR is triggered on the falling edge of GPIO pin PC13, which
#           is attached to the on-board button. The routine stores the current
#           timer count in the global variable, endCount. 
#  @param param An unused parameter.
#  @return This function does not return anything, as it is a callback function.
def button_isr(param):                  # Define ISR
    global pressed   
    pressed = True

## @brief Set up a hardware interrupt, attached the falling edge of hardware pin PC13. 
extint = pyb.ExtInt(pyb.Pin.board.PC13, # GPIO pin PC13
             pyb.ExtInt.IRQ_FALLING,    # Interrupt on falling edge
             pyb.Pin.PULL_UP,           # Activate pullup resistor
             button_isr)                # Interrupt service routine

## @brief   State variable for the FSM.
#  @details This variable stores the current state of the FSM.
state = 0

while True:                                                                    # We will run our state machine in a forever loop
    try:                                                                       # If there are no keyboard interrupts... 
        if state == 0:                                                         # State 0 - Init
            state = 1                                                          # Nothing to do here, transition to state 1 
        elif state == 1:                                                       # State 1 - Wait for 'G' press from PC
            if PCcom.any() != 0:                                               # If there's anything in the serial line...
                val = PCcom.readchar()                                         #    Read the latest character      
                if str(val) == "103":                                          #    If the character was an ASCII 'G'...
                    PCcom.write("\nreceived")                                  #    Then acknowledge receipt back to PC
                    pressed = False                                            #    Make sure pressed is initialized to False
                    state = 2                                                  #    Transition to state 2
        elif state == 2:                                                       # State 2 - Wait for step response 
            # Here, we not only want to make sure the button was pressed, but we also want to make sure that there's something
            # useful in the buffer that we've been filling over and over again. To do this, we check the first and last values
            # of preBuff. If the first value of preBuff is below our transition threshold, but its last value is above it,
            # then it means that at some point in the middle of preBuff, a step response has begun. If this is the case,
            # then we want to continue taking ADC readings to capture the rest of the step response.
            if pressed and preBuff[0] < lowerThreshold and preBuff[-1] > lowerThreshold:  
                if ADCreader.read() < upperThreshold:                          #    If there is still more step-response to record...
                    ADCreader.read_timed(ADCbuff, tim2)                        #    Then fill the main buffer
                tim1.freq(5)                                                   #    Start blinking the LED faster
                state = 3                                                      #    Transition to state 3
            else:                                                              # If no step response was detected...
                ADCreader.read_timed(preBuff, tim2)                            #    Then, keep refilling the pre-buffer
        elif state == 3:                                                       # State 3 - Transmit data
            period = round(1000000/frequency) # Period, in microseconds        # Calculate period based on timer frequency
            exportADC.append(preBuff[0])                                       # Append first element before the loop begins
            for k in range(1, len(preBuff)):                                   # Do this for each element in preBuff...
                time.append(time[k-1]+period)                                  #    Append the next timestamp to time[]
                exportADC.append(preBuff[k])                                   #    Append the next ADC count to exportADC[]
            lowRange = len(time)                                               # Create local variable to set a new loop start datum
            if ADCbuff[0] > lowerThreshold:                                    # If there is useful data in the main buffer...
                for k in range(lowRange-1, len(time)+len(ADCbuff)-1):          #    Do this for each element of the main buffer... 
                    time.append(time[k-1]+period)                              #         Append the next timestamp
                    exportADC.append(ADCbuff[k-lowRange+1])                    #         Append the next ADC count   
            for k in range(len(time)):                                         # Use a for-loop to create single single string
                PCcom.write("\n"+str(time[k])+","+str(exportADC[k]))           # Transmit combined data over the serial connection 
            PCcom.write("\ndone")                                              # Send success message after finishing the data
            time = [0]                                                         # Reset time domain for next step response
            exportADC = []                                                     # Reset ADC counts for next step response
            tim1.freq(1)                                                       # Return LED to normal behavior
            state = 1                                                          # Return to state 1 to await next command from PC
        else:                                                                  # If we somehow got into an impossible state...
            pass                                                               #     Do nothing
    except KeyboardInterrupt:                                                  # If a keyboard interrupt was pressed...
        break                                                                  #     Then, break out of the FSM to exit the program
                                                                               #
print("Exiting program...")                                                    # Print exit message  
print("Cntl+c was pressed, press Cntl+d to restart program.")                  #
