## @file Lab0x03.py
#  Contains a user interface for capturing the step response of an ADC reading
#  on the Nucleo-L476RG development board.  
#
#  This file contains an interface that allows the user to interact with the 
#  Nucleo board from the Spyder built-in console. When the user presses 'G' on
#  the keyboard, this script will tell the Nucleo board to begin taking ADC
#  readings. On the Nucleo, the user will press the built-in button. The 
#  Nucleo will record the step response in ADC counts and report back to the
#  interface, which will display a plot of the step response to the user in 
#  Spyder. This script saves recorded data to a CSV file in the same directory
#  in which this script was executed from. In order for this script to work
#  with the Nucleo-L476RG development board, the script titled, Lab0x03_uC.py
#  must be loaded onto the microcontroller as main.py. See Lab0x03_uC 
#  documentation for instructions on how to do this. Next, the module, pySerial
#  must be installed on the PC that this script is run from. To install this
#  module, open the Anaconda command prompt and type: pip install pySerial,
#  if it is not already installed. 
#
#  @author Jordan Kochavi
#
#  @copyright 2021
#
#  @date February 1, 2021
#
#  @section sec_3a Step Response Plot
#  The image below is an exported plot of a good step response measured by 
#  the Nucleo's ADC. The figure's title, "Step Response [9] vs. Time" is 
#  assigned by the software to correspond with the data file that contains the
#  raw ADC measurements for this particular step response. This data file is
#  available at: https://bitbucket.org/jkochavi/me405_labs/src/master/Lab3/Step_Response_Data_9.csv.
#  In the spreadsheet, the first column represents time stamps, in microseconds, 
#  and the right column represents ADC counts, before being converted to Volts.
#
#  <img src="https://bitbucket.org/jkochavi/me405_labs/raw/c16b62f3f90f934240b7a9ae083abf6125b4c48a/Lab3/step_response.png" alt="Step response measured by Nucleo"> 
#
#

## @brief   Module used to listen to the keyboard.
#  @details This module must be installed using the pip3 install command.
import keyboard

## @brief   Module used for creating and writing to CSV files.
import csv

## @brief   Module for serial communication.
#  @details This module is required for communication between the PC and Nucleo
#           development board. 
import serial

## @brief   Module to plot to make custom graphs and plots.
from matplotlib import pyplot as plt

## @brief   Module used to check whether a file exists or not.
from os import path

## @brief   Serial communication port.
#  @details Storing the chosen serial communication port as a global variable
#           allows for convinient changing if the user connects their 
#           microcontroller to a different port. Changing this variable, which
#           is stored as a string, will change the communication port.
comPort = "COM4"

## @brief   Baud rate for serial communication.
#  @details Storing the baud rate ags a global variable allows for convinient
#           changing if the user posesses a slightly different microcontroller.
#           The STM32 works well with a baud rate of 115200, however many 
#           microcontrollers require different baud rates for serial communication.
#           Changing this variable, which is stored as an integer, will change
#           the serial communication baud rate.
baud = 115200

## @brief   File name for storing data from Nucleo board.
#  @details Storing the file name in global variable allows for convinient 
#           changing if the user wishes to name the file differently. 
fileName = "Step_Response_Data"

## @brief   State variable for the FSM.
#  @details This variable stores the current state of the FSM.
state = 0

## @brief   Unique number attached to each CSV file.
#  @details This variable is concatenated with the fileName variable in the
#           format, Step_Response_Data_X.csv, where X is the fileNumber. This
#           allows the user to repeatedly run the program, and have data store
#           automatically to a new file without erasing any other data. Another
#           solution could be to add new columns to a single spreadsheet, but
#           this method works well for a state machine because we can simply
#           increment this variable to get a new file for the next step response.
fileNumber = 0

## @brief   List that stores ADC counts from the Nucleo board.
#  @details This variable is initialized as an empty list. When the Nucleo
#           board records a step response, it transmits the ADC counts over UART.
#           Each ADC count is appended to this list, which is subsequently 
#           converted to Volts for plotting, and stored in the CSV file as ADC counts.
ADCdata = []

## @brief   List that stores time stamps from the Nucleo board.
#  @details This variable is initialized as an empty list. In addition to the ADC
#           counts, the Nucleo board also transmits a linear domain of time stamps.
#           The timestamps are calculated based on the frequency at which ADC
#           readings are taken. For example, if the Nucleo takes readings at 200KHz,
#           that translates to a period of 5 microseconds. Therefore, timestamps
#           begin at zero, an increment by 5 microseconds. 
timeData = []

## @brief   A function that prints debugging tips and instructions for usage.
#  @details This function only consists of a series of print statements that
#           tell the user information about how to approach different hardware
#           problems. It also describes the Nucleo's LED behavior, which is 
#           also used to convey information to the user.
def printInstructions():
    print("To ensure hardware is operating correctly:")
    print("*** Make sure Nucleo-L476RG is loaded with correct main.py file.")
    print("*** Look for steadily flashing green light, which indicates normal operation.")
    print("If there is not a flashing green light:")
    print("*** Press cntl+c to exit this program.")
    print("*** Open a serial monitor in Putty at "+comPort)
    print("*** Press cntl+d to soft-reboot the Nucleo.")
    print("*** Close Putty and re-run this program in Spyder.")
    print("\nTo begin taking ADC readings, press 'G'.")
    print("The green LED will flash quickly during data transmission.")

## @brief   A function that separates a comma delimited string into two integers.
#  @details After the Nucleo board records a step response, it combines timestamps
#           and ADC readings together, and sends one timestamp and ADC reading at 
#           a time back to the PC. These values are transmitted through serial 
#           communication and are received by the PC as a string. For example,
#           the output of the Nucleo may read: 4050,2330, where 4050 is the 
#           timestamp, in microseconds, and 2330 is the ADC count. This function
#           takes the string, "4050,2330", and separates it into 4050 and 2330 
#           using the find() function to identify the index where a comma appears.
#           Next, each value is converted from a string to an integer, and appended
#           to timeData and ADCdata for plotting and storing. 
#  @param data A string that contains two values separated by a comma.
#  @return listData A list containing two separated strings. The results are 
#           returned as a string to be written to the CSV file.
def parseData(data):                                      # Function definition
    global ADCdata, timeData                              # Reference global variables
    index = data.find(",")                                # Identify the index where a comma appears
    listData = [data[0:index],data[index+1:len(data)-1]]  # Separate the string based on the location of the comma
    try:                                                  # Just in case a letter slipped through...
        timeData.append(int(data[0:index]))               # Convert the timestamp to an integer and append to the list
        ADCdata.append(int(data[index+1:len(data)-1]))    # Convert the ADC count to an integer and append to the list
    except:                                               # If there are any errors...
        pass                                              #     Do nothing
    return listData                                       # Return a list of both values, as strings.

## @brief   Function that converts a list of ADC counts into voltage.
#  @details This function receives a list of ADC counts that were transmitted
#           from the Nucleo board, and converts them to voltages based on the 
#           specifications of the Nucleo's ADC. The Nucleo has a 12 bit ADC, 
#           which means that its maximum ADC reading is 2^12 = 4096. The Nucleo's
#           maximum voltage input into its ADC is 3.3V, which means that an ADC
#           reading of 4096 corresponds to a 3.3V analog voltage input. We can 
#           calculate the analog voltage for a given ADC reading by dividing it
#           by the ratio of maximum ADC count to maximum voltage input. 
#  @param data A list of ADC counts.
#  @return voltage A list of converted voltages.
def convertADCtoVoltage(data):                            # Fuction definition
    voltage = []                                          # Initialize empty list of voltages
    for k in range(len(data)):                            # Do this for each element of the input list...
        volts = data[k]/(4096/3.3)                        # Convert to volts
        voltage.append(volts)                             # Append to the list of voltages
    return voltage                                        # Return the list of voltages

while True:                                                                    # Our state machine will run in a forever loop 
    try:                                                                       # As long as there are no keyboard interruptions...   
        if state == 0:                                                         # State 0 - Init
            global ser                                                         # Reference global variable 
            try:                                                               # Try to establish a serial connection 
                ser = serial.Serial(port=comPort, baudrate=baud, timeout=1)    # If it is successfull...
                print("Serial connection established at "+comPort)             #    Then, display success message
            except:                                                            # If connection unsueccessful... 
                print("Serial connection could not be established at "+comPort)#    Then, display error message
                print("Please check connections and restart the program.")     #    Provide debugging instructions
                break;                                                         #    Exit forever loop
            validFile = False                                                  # Initialize to False 
            while not validFile:                                               # As long as there is no valid file name available...
                if path.exists(fileName+"_"+str(fileNumber)+".csv"):           #    If a specific filename already exists...
                    fileNumber += 1                                            #         Then change it slightly and try again 
                else:                                                          #    Otherwise...
                    validFile = True                                           #         We have a valid file! 
            printInstructions()                                                # Display welcome instructions
            state = 1                                                          # Transition to state 1 
        elif state == 1:                                                       # State 1 - Wait for press
            if keyboard.is_pressed("g"):                                       # If the 'g' key was pressed... 
                state = 2                                                      #    Then, transition to state 2
        elif state == 2:                                                       # State 2 - Wait for release
            if not keyboard.is_pressed("g"):                                   # If the 'g' key is no longer being pressed... 
                print("Waiting for response...")                               #    Then notify the user of transmision to Nucleo
                ser.write("\ng".encode("ascii"))                               #    Write a 'g' ASCII character to the Nucleo
                state = 3                                                      #    Transition to state 3
        elif state == 3:                                                       # State 3 - Wait for acknowledgement from Nucleo
            receipt = ser.readline().decode("ascii")                           # Check for acknowledgement message
            if receipt == "received":                                          # If acknowledgement message has been received...
                print("Hardware ready...")                                     #    Then print success and next instructions
                print("Press the blue user button to generate a step response.")
                print("Spreadsheet with results will be created with name: "+fileName+"_"+str(fileNumber)+".csv")
                ADCdata = []                                                   # Initialize to empty before data storage
                timeData = []                                                  # Initialize to empty before data storage
                state = 4                                                      # Transition to state 4
        elif state == 4:                                                       # State 4 - Store data
            val = ser.readline().decode("ascii")                               # Retrieve next line from the Nuleo
            if val == "done":                                                  # If the Nucleo says 'done', it means it has finished transmitting
                print("Data transfer complete!")                               #     Print success message
                state = 5                                                      #     Transition to state 5   
            elif val != "":                                                    # Elif the Nucleo says something else...
                currentRow = parseData(val)                                    #     Then, parse the line into two values
                with open(fileName+"_"+str(fileNumber)+".csv","a",newline="") as f:# Create and/or open the spreadsheet file 
                    dataFile = csv.writer(f)                                   #     Create a writer object
                    dataFile.writerow([currentRow[0],currentRow[1]])           #     Append the latest row of time/ADC data
        elif state == 5:                                                       # State 5 - Plot data
            ADCvoltage = convertADCtoVoltage(ADCdata)                          # Convert the ADC counts into voltages   
            plt.plot(timeData,ADCvoltage)                                      # Create a plot of voltage vs. time
            plt.title("Step Response ["+str(fileNumber)+"] vs. Time")          # Title the graph that corresponds to the fileName
            plt.xlabel("Time [microseconds]")                                  # Label the x-axis
            plt.ylabel("Analog Voltage [V]")                                   # Label the y-axis
            plt.show()                                                         # Display the plot in Spyder's plot window
            fileNumber += 1                                                    # Increment file count for next step reading
            print("\nPress 'G' to begin again...\n")                           # Prompt next instruction to user
            state = 1                                                          # Return to state 1 to begin again!
        else:                                                                  # In case we wind up in an impossible state...
            pass                                                               #    Do nothing 
    except KeyboardInterrupt:                                                  # If a keyboard interrupt was entered
        break                                                                  # Break out of the loop to exit the program
                                                                               #
print("Exiting program...")                                                    # Print exit message
print("To restart:")                                                           # Display instructions for resetting
print("*** Open Putty and soft-reboot the Nucleo")                             #
print("*** Restart this program in Spyder")                                    #
try:                                                                           # If a serial connection was opened...
    ser.close()                                                                #    Then close it
except:                                                                        # Otherwise...
    pass                                                                       #    Do nothing and exit 