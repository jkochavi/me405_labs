## @file Lab0x02.py
#  Contains a reaction-testing script using a built in LED and push button
#  on the Nucleo-L476RG development board. 
#
#  The built-in LED will be toggled on and off using one of the STM32's timers.
#  The LED remains on for one second, and remains off for a random amount
#  of seconds, ranging between 2 and 3 seconds. Hardware interrupts are used 
#  to detect the falling edge of a button push. When the button is pushed, 
#  the number of milliseconds passed since the LED was turned on is returned
#  to the console. To load this file onto the Nucleo-L476RG, open the Anaconda
#  command line and type: ampy --port <device port> put Lab0x02.py main.py, 
#  where <device port> is whichever serial port the Nucleo is plugged into, 
#  i.e. COM4. Ensure that this file is the command line's working directory.
#  The output of this program can be viewed in any serial monitor, such as 
#  Putty. 
#
#  @author Jordan Kochavi
#
#  @copyright 2021
#
#  @date January 24, 2021
#
#

## @brief Import the py-board module used to interface with the STM32's hardware.
import pyb
## @brief Import a micropython module used to generate a random number.
import urandom

## @brief   A boolean flag raised to store timer count outside of ISR.
#  @details This variable is raised in the button's interrupt service routine,
#           which indicates to the foreground loop that a new response time 
#           needs to be appended to the list of response times. Since ISR's 
#           should not handle memory allocation, toggling a single variable is 
#           a time and memory-efficient way to store the response time. 
saveCount = False
## @brief   A boolean flag that prevents double-pressing.
#  @details We only want to allow the user to press the button once during an 
#           LED on period. Otherwise, the user could press the button many 
#           times while the LED is on, and it would log several increasing
#           response times, which would dilute the average response time--we 
#           only really care about the first press. This variable is toggled
#           off in the ISR after the timer count is recorded.
allowPress = True

## @brief   Integer that stores the amount of microseconds the LED is on for.
#  @details This variable stores the amount of microseconds that the LED 
#           remains on for. Storing this value in a global variable makes it
#           easy to change, should we want to change its period later on. 
timeOn = 1000000

## @brief   Integer that stores the amount of microseconds the LED is off for.
#  @details The LED is to remain off for a random amount of time, ranging 
#           between 2 and 3 seconds. This variable is set using the urandom
#           module. The randrange() function returns a random number between
#           a specified range. This variable is reassigned each period to a 
#           new random number. 
timeToOn = urandom.randrange(2000000,3000000)

## @brief   Integer that stores a specific value of the timer counter.
#  @details This variable stores the timer counter value when user presses the
#           button. It is initialized as a 32-bit integer to avoid python's 
#           default allocation of 31 bits. 
endCount = 0xFFFFFFFF

## @brief   List that stores all recorded user response times, in microseconds.
#  @details Whenever the user presses the button, the recorded response time 
#           is appended to this list. When the user presses cntl+c to exit the
#           program, this variable is passed as parameter to the function,
#           calculateAverage(), which returns the average response time to the
#           console.
responseTimes = []

## @brief   Initialize the on-board LED that is attached to GPIO pin PA5.
onBoardLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP) # Configure for output, pullup
onBoardLED.low()                                             # Initialize LED to off

## @brief   Create a timer object for timer hardware 2. 
tim2 = pyb.Timer(2)                            
tim2.init(prescaler=79, period=0x7FFFFFFF)    # Initialize the timer with prescaler of 79

## @brief   Callback for end of timer period.
#  @details This function is triggered when the timer reaches its maximum period
#           of 0x7FFFFFFF. Because of how the main loop works, it is unlikely that 
#           the timer would overflow, since it is constantly being reset to 0. 
#           However, if it were to overflow, we would not want to allow the 
#           user to press the button until the timer is working normally again.
#           Therefore, we just turn off the flag, allowPress, which will prevent
#           a reaction time to be logged until the main loop raises it again.
#  @param timer An unused parameter. 
#  @return This function does not return anything, as it is a callback function.
def ovf_callback(timer):                      # Define callback
    global allowPress                         # Reference global variable
    allowPress = False                        # Force allowPress to be False

tim2.callback(ovf_callback)                   # Assign ovf_callback as timer callback function

## @brief   An interrupt service routine for the on-board button.
#  @details This ISR is triggered on the falling edge of GPIO pin PC13, which
#           is attached to the on-board button. The routine stores the current
#           timer count in the global variable, endCount. 
#  @param param An unused parameter.
#  @return This function does not return anything, as it is a callback function.
def button_isr(param):                        # Define ISR
    global allowPress                         # Reference global variable
    if onBoardLED.value() and allowPress:     # If the LED is on and we want to accept a press...
        global endCount, saveCount            #     Reference global variables
        endCount = tim2.counter()             #     Store timer count in global variable
        allowPress = False                    #     Prevent additional presses
        saveCount = True                      #     Raise flag to append count to list
    else:                                     # Else...
        pass                                  #     Do nothing
        
## @brief Set up a hardware interrupt, attached the falling edge of hardware pin PC13. 
extint = pyb.ExtInt(pyb.Pin.board.PC13,       # GPIO pin PC13
             pyb.ExtInt.IRQ_FALLING,          # Interrupt on falling edge
             pyb.Pin.PULL_UP,                 # Activate pullup resistor
             button_isr)                      # Interrupt service routine

## @brief   Function that returns the average value from a list.
#  @details This function receives a list as a parameter, and uses a for-loop
#           to calculate the average value of all the elements of the list. 
#           If the list is empty, then the function returns None. Otherwise,
#           it returns an integer containing the average value.
#  @param   listParam The list of values to be averaged.
#  @return  If listParam is an empty list, then the function returns None. 
#           Otherwise, it returns an integer containing the average value.
def calculateAverage(listParam):              # Define function
    if len(listParam) > 0:                    # If the list isn't empty...
        avg = 0                               #     Create local variable 
        for k in range(len(listParam)):       #     For each element in the list...
            avg += listParam[k]               #          Add each value to running sum
        avg /= len(listParam)                 #     Calculate average
        return avg                            #     Return the average
    else:                                     # Otherwise, the list is empty...
        return None                           #     So return None

# A forever loop to run our LED on-off loop.
while True:                                                         # Do this forever...
    try:                                                            # If no programming/keyboard errors...
        if saveCount:                                               # If we need to save a recorded response time...
            responseTimes.append(endCount)                          #      Append it to the list
            print("Press logged")                                   #      Return feedback to the user
            saveCount = False                                       #      Lower flag for next press
        elif tim2.counter() > timeToOn and not onBoardLED.value():  # Else if it's time to turn the LED on...
            onBoardLED.high()                                       #      Turn on the LED
            tim2.counter(0)                                         #      Reset the timer to begin counting to 1 second
            allowPress = True                                       #      Begin allowing presses to receive response time
        elif tim2.counter() > timeOn and onBoardLED.value():        # Else if it's time to turn the LED off...
            onBoardLED.low()                                        #      Turn off the LED
            tim2.counter(0)                                         #      Reset the timer to begin counting off period
            timeToOn = urandom.randrange(2000000,3000000)           #      Reset off period to random time
    except KeyboardInterrupt:                                       # Raise exception for keyboard interrupt (cntl+c)
        break                                                       #      Break out of the loop

## @brief   Variable that stores average user response time.
#  @details This variable calls the calculateAverage() function, passing the 
#           list of response times as a parameter.
averageResponseTime = calculateAverage(responseTimes)                          # Assign variable
if averageResponseTime != None:                                                # If an average was calculated...
    print("Average response time: ",round(averageResponseTime/1000)," [ms]")   #     Then, print it to the user
else:                                                                          # Otherwise...
    print("Reaction time not successfully measured...")                        #     Print error message
print("Press cntl+d to start again")                                           # Prompt user to begin again
