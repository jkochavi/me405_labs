'''
@file mcp9808.py
@brief      This file contains the methods to interface with an MCP9808 object.
@details    This script contains a class and methods for the MCP9808 temperature
            sensor. The sensor module, produced by Adafruit, communicates with
            the Nucleo L476RG using I2C, and carries the I2C address of 0x18.
@author     Giselle Morales
@author     Jordan Kochavi
@date       February 4th, 2021 Shell/skeleton written
@date       February 8th, 2021 Temperature reading functions added

@package    mcp9808
@brief      An I2C temperature-reading module.
@details    This module communicates with an MCP9808 integrated circuit on 
            a breakout board produced by Adafruit. This module retrieves the
            ambient temperature and may return the value in Celsius and/or 
            Fahrenheit.
@author     Giselle Morales
@author     Jordan Kochavi
@date       February 4th, 2021 Shell/skeleton written
@date       February 8th, 2021 Temperature reading functions added  

'''

## Import the pyb module used to connect to I2C bus.
from pyb import I2C
## Import module for time delays and measurements.
import utime

class MCP9808:
    '''
    @brief      Allows user to communicate with an MCP9808 temperature sensor using its I2C interface.
    @details    This module takes in
    @author     Giselle Morales
    @author     Jordan Kochavi
    @date       February 4th, 2021 Shell/skeleton written
    @date       February 8th, 2021 Temperature reading functions added
    '''
    def __init__(self, I2C_object, address=0x18):
        '''
        @brief              Class object initializer 
        @details            This initializer is ran when an MCP9808 object
                            is instantiated. It assigns the sensor's I2C address
                            based on the Adafruit module's datasheet.  
        @param I2C_object   The I2C object used for communication between MCU and MCP9808
        @param address      The bus address corresponding to the temperature sensor.
                            This value is 0x18 by default, as this address is specified
                            in the module's datasheet. However, if the user has a 
                            slightly different module with a different I2C address,
                            then they may pass it as an argument.
        '''
        ## Create the IC2 object
        self.I2C_obj = I2C_object
        ## Create the IC2 bus address 
        self.address = address
        ## Initialize I2C object
        self.I2C_obj.init(I2C.MASTER, baudrate= 400000)
    def check(self):
        '''
        @brief   Function that verifies I2C bus connection. 
        @details This function may be checked once or periodically to test the
                 sensor's connection on the I2C bus. It calls the I2C function,
                 is_ready(), to check if the sensor responds to the bus at its
                 specific address. If the correct I2C address is connected to
                 the bus, then this function reads the internal register that 
                 represents the manufacturer ID. From the MCP9808 datasheet,
                 this is internal register 0x06, which is a 16-bit, 2-byte 
                 register. The datasheet specifies that the contents of this 
                 register should be 84 in decimal. This function converts the
                 2-byte reading into an integer using the function, from_bytes()
                 and compares it to 84. If the ID is in fact, 84 then this function
                 returns true. 
        @return  This function returns True if the device is connected to the 
                 I2C bus, and returns False if the device is unresponsive. This
                 function also returns False if the ID read from register 6 does
                 not match the manufacturer ID.
        '''
        if self.I2C_obj.is_ready(self.address):                # If the sensor is connected...
            data = self.I2C_obj.mem_read(2, self.address, 0x06)# Read 2 bytes from internal register 0x06    
            intData = int.from_bytes(data,'big')               # Convert to integer
            if intData == 84:                                  # If the integer matches the manufacturer ID...
                return True                                    #     Return true
        return False                                           # Otherwise... return False
    def celsius(self):
        '''
        @brief   Measures ambient temperature, in Celsius
        @details This function reads the internal register on the MCP9808, which
                 contains the measured ambient temperature. The register is 
                 specified as register 0x05 on the MCP9808, which is a 16-bit/
                 2-byte register. This function reads 2 bytes from the register
                 and converts them into a floating point number that represents
                 the measured temperature, in Celsius. After receiving 2 bytes
                 of data, this function converts them to an integer value using
                 the Python function, from_bytes(). From the MCP9808 datasheet,
                 the 13, 14, and 15 bits of the register contain comparators
                 that toggle high or low to indicate other additional functionality.
                 Since those bits do not relate to the temperature measurement,
                 they are masked out with the binary number, 0b0001111111111111,
                 which translates to 8191 in decimal. Next, we separate the data
                 into its high and low bytes. The low byte is obtained by masking
                 the 2-byte number with 0b0000000011111111, which translates to 
                 255 in decimal. The high byte is obtained by right-shifting the
                 2-byte number by 8 bits to isolate the 8 high bits. 
                 
                 Finally, the temperature is calculated using the formula
                 specified in the MCP9808 datasheet and returned. 
        @return  A floating point number containing the measured temperature in Celsius.
        '''        
        data = self.I2C_obj.mem_read(2, self.address, 0x05)    # Read 2 bytes from internal register 0x05
        intData = int.from_bytes(data,'big')                   # Convert both bytes to a signed integer
        intData = intData & 8191                               # Mask the indicator bits
        LSB = intData & 255                                    # Store low byte by masking 255
        MSB = intData >> 8                                     # Store high byte by right shifting 8
        return MSB*16 + LSB/16                                 # Calculate and return temperature, in (+) Celsius
       
    def fahrenheit(self):
        '''
        @brief   Converts measured temperature to Fahrenheit.
        @details This function first retrieves the measured temperature using
                 the method, celsius(), and then converts it to Fahrenheit.
        @return  A floating point number containing the measured temperature in Fahrenheit.
        '''
        temperature = self.celsius()                          # Retrieve temperatyre, in Celsius
        return temperature*(9/5) + 32                         # Return temperature, in Fahrenheit
    
if __name__== '__main__':                                     # Test code...
    i2c = I2C(1)                                              # Create I2C object on I2C channel 1
    tempSensor = MCP9808(i2c)                                 # Create MCP9808 object
    state = 0                                                 # Initialize state variable
    while True:                                               # In a forever loop... 
        try:                                                  # If no keyboard interrupt...
            if state == 0:                                    # State 0 - Check sensor connection
                 if tempSensor.check():                       # If sensor connected...
                     print("Sensor ID connected and matches.")#     Print success message
                     state = 1                                #     Transition to state 1 
            elif state == 1:                                  # State 1 - Take measurements every second
                temp_C = round(tempSensor.celsius(),2)        # Get temperature in Celcius
                temp_F = round(tempSensor.fahrenheit(),2)     # Get temperature in Fahrenheit
                print(str(temp_C)+" [C],"+str(temp_F)+" [F]") # Print results
                utime.sleep(1)                                # Delay 1 second
        except KeyboardInterrupt:                             # If keyboard interrupt...
            break                                             #     Break loop
    print("Exiting measurment testing")                       # Print exit message